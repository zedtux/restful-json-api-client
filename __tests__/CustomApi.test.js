import fetchMock from 'fetch-mock'

import RestfulClient from '..'

const API_URL = 'https://api.domain.co/api'

let positionsApi
let positionsApiFetch

describe('RestfulClient CRUD', () => {
  beforeAll(() => {
    // In this test file, we don't care about the response
    fetchMock.mock('*', { status: 200 })

    class PositionsApi extends RestfulClient {
      constructor (authToken) {
        super(API_URL, {
          resource: 'positions',
          headers: {
            // Include as many custom headers as you need
            Authorization: `JWT ${authToken}`
            // Content-Type: application/json
            // and
            // Accept: application/json
            // are added by default
          }
        })
      }

      getWeather (date) {
        // The body object will be used to build a query.
        // For example, in the case `date` is `{ "lt": "2018/07/13" }` the GET query
        // will be https://api.myapp.com/weather?lt="2018/07/13"
        return this.request('GET', { path: 'weather', body: { date } })
      }

      checkIn (lat, lon) {
        return this.request('POST', { path: 'checkin', body: { lat, lon } })
      }
    }

    positionsApi = new PositionsApi('this-is-my-auth-token')
    positionsApiFetch = jest.spyOn(positionsApi, '_fetch')
  })

  afterAll(() => {
    fetchMock.reset()
  })

  describe('getWeather()', () => {
    beforeEach(() => {
      positionsApi.getWeather({ "lt": "2018/07/13" })
    })

    it('should call only one time the fetch function', () => {
      return expect(positionsApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a GET request to /weather', () => {
      return expect(positionsApiFetch).toHaveBeenCalledWith(
        `${API_URL}/positions/weather?date%5Blt%5D=2018%2F07%2F13`, {
          headers: {
            "Accept": "application/json",
            "Authorization": "JWT this-is-my-auth-token",
            "Content-Type": "application/json"
          },
          method: "GET"
        }
      )
    })
  })

  describe('checkIn()', () => {
    beforeEach(() => {
      positionsApi.checkIn(48.4597, -5.0862)
    })

    it('should call only one time the fetch function', () => {
      return expect(positionsApiFetch).toHaveBeenCalledTimes(1)
    })

    it('should send a GET request to /weather', () => {
      return expect(positionsApiFetch).toHaveBeenCalledWith(
        `${API_URL}/positions/checkin`, {
          body: JSON.stringify({
            lat: 48.4597,
            lon: -5.0862
          }),
          headers: {
            "Accept": "application/json",
            "Authorization": "JWT this-is-my-auth-token",
            "Content-Type": "application/json"
          },
          method: "POST"
        }
      )
    })
  })
})
